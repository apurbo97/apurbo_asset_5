import random

# Boolean data uppercase,numbers,specialchars (True/False)
#charlen number


def password(uppercase,numbers,specialchars,charlen):
    characters = list('abcdefghijklmnopqrstuvwxyz')

    if uppercase:
        characters.extend('ABCDEFGHIJKMNOPQRSTUVWXYZ')
    
    if numbers:
        characters.extend('0123456789')
    
    if specialchars:
        characters.extend('!@#$%^)(*&')

    thepassword = ''
    length = int(charlen)
    for x in range(length):
        thepassword += random.choice(characters)
    return thepassword